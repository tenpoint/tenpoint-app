export const AppLogo = require('./images/tenpoint-circle-logo.png')
export const TenpointCircle = require('./images/tenpoint-circle-multi-color.png')

// Sample
export const SampleImages = {
  profile: require('./images/profile-photo-1.jpg'),
  promo: require('./images/tot-promo-bread-juice.png')
}
