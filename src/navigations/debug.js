import React, { useContext } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { setNavigator } from './navigationRef'
import SignupScreen from '../screens/SignupScreen'
import SigninScreen from '../screens/SigninScreen'
import MainNavigator from './MainNavigator'
import AuthNavigator from './AuthNavigator'
import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'

function App() {
  const Stack = createStackNavigator()
  const Tab = createBottomTabNavigator()
  const {
    state: { t }
  } = useContext(LocalizationContext)

  return (
    <NavigationContainer
      ref={navigator => {
        setNavigator(navigator)
      }}>
      <Stack.Navigator>
        <Stack.Screen
          name='Signup'
          component={SignupScreen}
          options={{
            title: t('SIGNUP_HEADER')
          }}
        />
        <Stack.Screen name='Signin' component={SigninScreen} />
        <Stack.Screen
          name='Main'
          component={MainNavigator}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
