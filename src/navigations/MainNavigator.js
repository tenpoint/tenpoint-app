import React from 'react'
import { View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
  faHome,
  faQrcode,
  faUserAlt,
  faReceipt
} from '@fortawesome/pro-light-svg-icons'

import HomeScreen from '../screens/HomeScreen'
import ScannerScreen from '../screens/ScannerScreen'
import AccountScreen from '../screens/AccountScreen'
import TransactionScreen from '../screens/TransactionScreen'

import ScannerNavigator from './ScannerNavigator'
import AccountNavigator from './AccountNavigator'

const Tab = createBottomTabNavigator()

export default () => {
  return (
    <Tab.Navigator
      tabBarPosition='bottom'
      swipeEnabled={true}
      animationEnabled={true}
      indicatorStyle={{ backgroundColor: '#00CD6B' }}
      tabBarOptions={{
        showIcon: true,
        showLabel: false,
        activeTintColor: '#00CD6B'
      }}>
      <Tab.Screen
        name='Home'
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <FontAwesomeIcon icon={faHome} color={color} size={32} />
          )
        }}
      />
      <Tab.Screen
        name='Scanner'
        component={ScannerNavigator}
        options={{
          tabBarLabel: 'Scanner',
          tabBarIcon: ({ color, size }) => (
            <FontAwesomeIcon icon={faQrcode} color={color} size={28} />
          )
        }}
      />
      <Tab.Screen
        name='Transaction'
        component={TransactionScreen.screen}
        options={{
          tabBarLabel: 'Transaction',
          tabBarIcon: ({ color, size }) => (
            <FontAwesomeIcon icon={faReceipt} color={color} size={28} />
          )
        }}
      />
      <Tab.Screen
        name='Account'
        component={AccountScreen.screen}
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: ({ color, size }) => (
            <FontAwesomeIcon icon={faUserAlt} color={color} size={28} />
          )
        }}
      />
    </Tab.Navigator>
  )
  // Home: HomeScreen,
  // Scanner: ScannerScreen,
  // Account: AccountScreen
}

// export default TabNavigator
