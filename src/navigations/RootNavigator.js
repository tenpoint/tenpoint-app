import React from 'react'
import { NavigationContainer, createSwitchNavigator } from 'react-navigation'

import AuthNavigator from './AuthNavigator'
import MainNavigator from './MainNavigator'

import HomeScreen from '../screens/HomeScreen'
import SigninScreen from '../screens/SigninScreen'
import { createStackNavigator } from 'react-navigation-stack'

//const RootNavigator = createSwitchNavigator({
//  Auth: AuthNavigator,
//  Main: MainNavigator
//})
//
//export default RootNavigator
