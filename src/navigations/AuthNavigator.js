import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import SigninScreen from '../screens/SigninScreen'
import SignupScreen from '../screens/SignupScreen'
import PhoneVerificationScreen from '../screens/PhoneVerificationScreen'

const Stack = createStackNavigator()

export default () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Signup' component={SignupScreen} />
      <Stack.Screen
        name='PhoneVerification'
        component={PhoneVerificationScreen}
      />
    </Stack.Navigator>
  )
}
