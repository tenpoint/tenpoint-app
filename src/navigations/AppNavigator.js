import React, { useContext } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { setNavigator } from './navigationRef'
import SignupScreen from '../screens/SignupScreen'
import SigninScreen from '../screens/SigninScreen'
import MainNavigator from './MainNavigator'
import AuthNavigator from './AuthNavigator'
import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { Context as AuthContext } from '../context/AuthContext'

function App() {
  const Stack = createStackNavigator()
  const Tab = createBottomTabNavigator()
  const { isSignedIn } = useContext(AuthContext)
  const {
    state: { t }
  } = useContext(LocalizationContext)

  return (
    <NavigationContainer
      ref={navigator => {
        setNavigator(navigator)
      }}>
      <Stack.Navigator>
        {!isSignedIn ? (
          <Stack.Screen
            name='Auth'
            component={AuthNavigator}
            options={{
              headerShown: false
            }}
          />
        ) : (
          <Stack.Screen
            name='Main'
            component={MainNavigator}
            options={{ headerShown: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
