import React, { useContext } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import Animated from 'react-native-reanimated'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import Header from '../components/Header'
import QRCodeScreen from '../screens/QRCodeScreen'
import ScannerScreen from '../screens/ScannerScreen'

const Tab = createMaterialTopTabNavigator()

const TabBar = ({ state, descriptors, navigation, position }) => {
  return (
    <View
      elevation={10}
      style={{
        marginTop: getStatusBarHeight(),
        height: '20%',
        backgroundColor: '#00CD6B',
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
          height: 2,
          width: 2
        }
      }}>
      <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            color: '#FFF',
            fontFamily: 'bookman',
            fontSize: 30,
            fontWeight: '500',
            marginTop: 2
          }}>
          tenpoint
        </Text>
      </View>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key]
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name

          const isFocused = state.index === index

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true
            })

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name)
            }
          }

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key
            })
          }

          const inputRange = state.routes.map((_, i) => i)
          const opacity = Animated.interpolate(position, {
            inputRange,
            outputRange: inputRange.map(i => (i === index ? 1 : 0))
          })

          return (
            <TouchableOpacity
              accessibilityRole='button'
              accessibilityStates={isFocused ? ['selected'] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              key={label}
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center'
              }}>
              <Text
                style={{
                  fontSize: 18,
                  color: '#FFF',
                  marginBottom: 5,
                  opacity: isFocused ? 1 : 0.7
                }}>
                {label}
              </Text>
              <View
                style={{
                  backgroundColor: isFocused ? '#FFF' : 'transparent',
                  height: 2,
                  borderTopRightRadius: 5,
                  borderTopLeftRadius: 5,
                  width: '40%'
                }}></View>
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

export default () => {
  const {
    state: { t }
  } = useContext(LocalizationContext)

  return (
    <Tab.Navigator
      tabBar={props => <TabBar {...props} />}
      indicatorStyle={{ backgroundColor: '#00CD6B' }}>
      <Tab.Screen
        name='QRCode'
        component={QRCodeScreen}
        options={{
          tabBarLabel: 'QR Code'
        }}
      />
      <Tab.Screen
        name='Scanner'
        component={ScannerScreen}
        options={{ tabBarLabel: t('SCANNER') }}
      />
    </Tab.Navigator>
  )
}
