import React from 'react'

import HeaderWithTabs from '../components/HeaderWithTabs'
import AccountScreen from '../screens/AccountScreen'
import TransactionScreen from '../screens/TransactionScreen'

export default () => {
  return (
    <HeaderWithTabs
      screens={[
        {
          name: AccountScreen.name,
          component: AccountScreen.screen,
          label: AccountScreen.label
        },
        {
          name: TransactionScreen.name,
          component: TransactionScreen.screen,
          label: TransactionScreen.label
        }
      ]}
    />
  )
}
