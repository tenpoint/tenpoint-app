import React, { useContext, useLayoutEffect } from 'react'
import {
  KeyboardAvoidingView,
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  ImageBackground
} from 'react-native'

//import { Input } from 'react-native-elements'
import PickerSelect from 'react-native-picker-select'

import { SafeAreaView } from 'react-navigation'
import {
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native-gesture-handler'

import Header from '../components/Header'
import { LanguageSelect } from '../components/LanguageSelect'
import { MainColors } from '../styles/colors'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { TenpointCircle, SampleImages } from '../../assets'

const name = 'Account'
const label = 'Account'

const screen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  return (
    <SafeAreaView style={{ flex: 1, width: '100%', backgroundColor: '#FFF' }}>
      <KeyboardAvoidingView
        style={{ flex: 1, width: '100%' }}
        behavior='padding'>
        <View style={styles.container}>
          <Header />
          <View style={styles.profileImageContainer}>
            <ImageBackground
              style={{
                width: 110,
                height: 110,
                padding: 5,
                alignSelf: 'center'
              }}
              source={TenpointCircle}>
              <Image
                source={SampleImages.profile}
                style={styles.profileImage}
              />
            </ImageBackground>
          </View>
          <View style={styles.profileDetailContainer}>
            <View style={styles.textInputContainer}>
              <Text style={styles.textInputLabel}>{t('NAME')}</Text>
              <TextInput style={styles.textInput} value='Bich Ngoc' />
            </View>
            <View style={([styles.textInputContainer], { marginTop: 10 })}>
              <Text style={styles.textInputLabel}>{t('MOBILE_NUMBER')}</Text>
              <TextInput style={styles.textInput} value='0936 409 808' />
            </View>
            <View style={([styles.textInputContainer], { marginTop: 10 })}>
              <Text style={styles.textInputLabel}>Email</Text>
              <TextInput style={styles.textInput} value='ngoc90ueb@gmail.com' />
            </View>
          </View>

          <LanguageSelect
            containerStyle={{
              marginTop: 10,
              width: '75%',
              alignSelf: 'center'
            }}
          />

          <View style={{ width: '75%', alignSelf: 'center', marginTop: 40 }}>
            <TouchableWithoutFeedback
              style={{
                alignSelf: 'center',
                borderWidth: 1,
                borderRadius: 20,
                padding: 8,
                width: '75%',
                alignItems: 'center',
                borderColor: MainColors.blue
              }}>
              <Text style={{ fontSize: 18, color: MainColors.blue }}>
                {t('SIGN_OUT_BTN_TEXT')}
              </Text>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  profileImageContainer: {
    padding: 3,
    marginTop: -44,
    width: 114,
    height: 114,
    borderRadius: 57,
    backgroundColor: '#FFF',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center'
  },
  profileDetailContainer: {
    width: '75%',
    alignSelf: 'center',
    marginTop: 20
  },
  textInputContainer: {
    marginTop: 5,
    marginBottom: 5
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.3)',
    color: 'rgba(0,0,0,0.7)',
    fontSize: 18,
    marginTop: 2
  },
  textInputLabel: {
    color: 'rgba(0,0,0,0.5)'
  }
})

const AccountScreen = { name: name, label: label, screen: screen }

export default AccountScreen

//<View style={{ marginTop: 10 }}>
//              <Text style={styles.textInputLabel}>Language</Text>
//              <PickerSelect
//                useNativeAndroidPickerStyle={false}
//                onValueChange={value => console.log(value)}
//                value='vn'
//                items={[
//                  { label: '🇻🇳 Tiếng Việt', value: 'vn' },
//                  { label: '🇺🇸 English', value: 'us' }
//                ]}
//                style={{
//                  paddingLeft: 0,
//                  marginTop: 5,
//                  fontSize: 20,
//                  inputAndroid: {
//                    fontSize: 18,
//                    color: 'rgba(0,0,0,0.7)'
//                  }
//                }}
//              />
//            </View>
