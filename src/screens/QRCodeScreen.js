import React, { useContext, useLayoutEffect } from 'react'
import { View, StyleSheet, Text } from 'react-native'

import QRCode from 'react-native-qrcode-svg'

import Header from '../components/Header'

const QRCodeScreen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  return (
    <View style={styles.container}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <QRCode
          backgroundColor='transparent'
          size={250}
          value='1XknNwxzcTk0vDyzYoYCTTU291S'
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  }
})

export default QRCodeScreen
