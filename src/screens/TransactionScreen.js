import React, { useContext, useLayoutEffect } from 'react'
import { View, StyleSheet, Text } from 'react-native'

import Header from '../components/Header'

const name = 'Transaction'
const label = 'Transaction'

const screen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  return (
    <View style={{ flex: 1 }}>
      <Header />
      <Text>Transaction</Text>
    </View>
  )
}

const TransactionScreen = { name: name, label: label, screen: screen }

export default TransactionScreen
