import React, { useContext, useLayoutEffect } from 'react'
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback
} from 'react-native'

import { Button, Header, Card, Input } from 'react-native-elements'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faFireAlt } from '@fortawesome/pro-regular-svg-icons'
import { faUsers } from '@fortawesome/free-solid-svg-icons'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height

const HomeScreen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  return (
    <View style={{ flex: 1 }}>
      <Header
        centerComponent={{
          text: 'tenpoint',
          style: {
            color: '#FFF',
            fontFamily: 'bookman',
            fontSize: 30,
            fontWeight: '500'
          }
        }}
        containerStyle={{
          height: '20%',
          alignItems: 'flex-start',
          marginTop: getStatusBarHeight(),
          backgroundColor: '#00CD6B'
        }}
      />
      <Card
        containerStyle={{
          width: '90%',
          borderRadius: 15,
          marginTop: '-15%',
          alignSelf: 'center',
          alignItems: 'center',
          padding: 4
        }}
        wrapperStyle={{ alignItems: 'center' }}>
        <View style={styles.topCardViewContainer}>
          <View style={styles.topCardCircleView}>
            <ImageBackground
              style={styles.topCardCircleImage}
              source={require('../../assets/tenpoint-circle-multi-color.png')}>
              <Text style={{ fontSize: 30, fontFamily: 'bookman' }}>1000</Text>
            </ImageBackground>
          </View>
          <View
            style={{
              width: '55%',
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <Text
              style={{
                fontFamily: 'bookman',
                fontSize: 20,
                textAlign: 'center'
              }}>
              You currently have 1000 tenpoint
            </Text>
          </View>
        </View>
      </Card>
      <ScrollView>
        <View style={styles.promoTitleContainer}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <FontAwesomeIcon icon={faFireAlt} color={'#FF4100'} size={18} />
            <Text style={styles.promoTitle}> Promotion</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Button
              type='clear'
              title='See All'
              containerStyle={{
                padding: 0,
                alignSelf: 'flex-end',
                marginRight: 5
              }}
              buttonStyle={{ padding: 0 }}
              titleStyle={{ padding: 0, textDecorationColor: '#0C7CC4' }}
            />
          </View>
        </View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={[{ id: '1' }, { id: '2' }, { id: '3' }]}
          keyExtractor={item => item.id}
          renderItem={({ item }) => {
            return (
              <TouchableWithoutFeedback style={{ margin: 0 }}>
                <Card containerStyle={styles.promoCardContainer}>
                  <View>
                    <Text>Promotion {item.id}</Text>
                  </View>
                </Card>
              </TouchableWithoutFeedback>
            )
          }}
        />
        <View style={styles.popularTitleContainer}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <FontAwesomeIcon icon={faUsers} color={'#00CD6B'} size={18} />
            <Text style={styles.popularTitle}> Popular</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Button
              type='clear'
              title='See All'
              containerStyle={{
                padding: 0,
                alignSelf: 'flex-end',
                marginRight: 5
              }}
              buttonStyle={{ padding: 0 }}
              titleStyle={{ padding: 0, textDecorationColor: '#0C7CC4' }}
            />
          </View>
        </View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={[{ id: '1' }, { id: '2' }, { id: '3' }]}
          keyExtractor={item => item.id}
          renderItem={({ item }) => {
            return (
              <TouchableWithoutFeedback style={{ margin: 0 }}>
                <Card containerStyle={styles.popularCardContainer}>
                  <View>
                    <Text>Popular {item.id}</Text>
                  </View>
                </Card>
              </TouchableWithoutFeedback>
            )
          }}
        />
      </ScrollView>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 100,
    backgroundColor: '#F7F6FB',
    width: '100%'
  },
  topCardViewContainer: {
    flexDirection: 'row',
    height: 0.15 * WINDOW_HEIGHT,
    width: 0.9 * WINDOW_WIDTH
  },
  topCardCircleView: {
    width: 0.9 * 0.45 * WINDOW_WIDTH,
    alignItems: 'center',
    justifyContent: 'center'
  },
  topCardCircleImage: {
    height: 0.145 * WINDOW_HEIGHT,
    width: 0.145 * WINDOW_HEIGHT,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  promoCardContainer: {
    width: 0.5 * WINDOW_WIDTH,
    height: 0.5 * WINDOW_WIDTH,
    borderRadius: 5,
    marginRight: 4,
    marginTop: 5,
    marginBottom: 5
  },
  promoTitleContainer: {
    marginTop: 10,
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center'
  },
  promoTitle: {
    fontSize: 17,
    fontWeight: '900',
    color: '#FF4100'
  },
  popularCardContainer: {
    width: 0.5 * WINDOW_WIDTH,
    height: 0.5 * WINDOW_WIDTH,
    borderRadius: 5,
    marginRight: 4,
    marginTop: 5,
    marginBottom: 5
  },
  popularTitleContainer: {
    marginTop: 10,
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center'
  },
  popularTitle: {
    fontSize: 17,
    fontWeight: '900',
    color: '#00CD6B'
  }
})

export default HomeScreen
