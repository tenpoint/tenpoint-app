import React, { useState, useContext, useLayoutEffect } from 'react'
import {
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  UIManager,
  View,
  Image,
  Text,
  TouchableWithoutFeedback
} from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { Card } from 'react-native-elements'

import { Input, Button, Icon } from 'react-native-elements'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { Context as AuthContext } from '../context/AuthContext'
import { LanguageSelect } from '../components/LanguageSelect'
import { AppLogo } from '../../assets'

const SignupScreen = ({ navigation }) => {
  const [phone, setPhone] = useState('')
  const { state, auth, signup, clearErrorMessage } = useContext(AuthContext)
  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        style={{ flex: 1, width: '100%' }}
        behavior='padding'>
        <View
          style={{
            flex: 2,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <Image style={{ width: 150, height: 150 }} source={AppLogo} />
          <Text
            style={{
              fontFamily: 'bookman',
              fontSize: 60,
              color: '#00CD6B',
              marginBottom: 15
            }}>
            tenpoint
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '400',
              marginTop: 25,
              color: 'rgba(0,0,0,0.6)'
            }}>
            {t('SIGN_IN_REGISTER_SCREEN_TITLE')}
          </Text>
          <Card
            containerStyle={{
              width: '90%',
              borderRadius: 15
            }}
            wrapperStyle={{ alignItems: 'center' }}>
            <Input
              label={t('MOBILE_NUMBER')}
              containerStyle={styles.phoneContainer}
              inputContainerStyle={styles.phoneInputContainer}
              inputStyle={styles.phoneInput}
              labelStyle={styles.phoneLabel}
              placeholder='09012345678'
              value={phone}
              onChangeText={setPhone}
              autoCorrect={false}
            />
          </Card>
          <Button
            title={t('CONTINUE')}
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            titleStyle={{ color: '#FFF' }}
            onPress={() => auth({ phone })}
          />
          <Text style={{ color: 'rgba(0,0,0,0.6)', marginTop: 5 }}>
            {t('I_ACCEPT_THE')}{' '}
            <Text style={{ color: '#00CD6B' }}>
              {t('TERMS_AND_CONDITIONS_TEXT')}
            </Text>
          </Text>
          <View style={{ width: '50%', alignItems: 'center' }}>
            <LanguageSelect
              containerStyle={{
                marginTop: 10,
                alignSelf: 'center',
                justifyContent: 'center'
              }}
              showLabel={false}
              backgroundColor='#F7F6FB'
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 55,
    backgroundColor: '#F7F6FB',
    width: '100%'
  },
  phoneContainer: {
    marginTop: 20,
    marginBottom: 20
  },
  phoneInputContainer: {
    //      borderBottomWidth: 0
    borderWidth: 1,
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.3)'
  },
  phoneInput: {
    textAlign: 'center'
  },
  phoneLabel: {
    fontWeight: '200',
    marginBottom: 5,
    marginLeft: 5
  },
  buttonContainer: {
    marginTop: 15,
    width: '75%',
    borderRadius: 20
  },
  button: {
    height: 40,
    //borderRadius: 20,
    backgroundColor: '#00CD6B',
    borderRadius: 20
  }
})

export default SignupScreen

//<LanguageDropdown
//        languages={i18n.getAvailableLanguageDropdown()}
//        onValueChange={changeAppLanguage}
//      />
//

//<Image
//            style={{ height: 133, width: 133 }}
//            source={require('../../assets/tenpoint_logo_133.png')}
//          />
