import React, { useContext, useLayoutEffect } from 'react'
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback
} from 'react-native'

import { Button, Card, Input } from 'react-native-elements'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faFireAlt } from '@fortawesome/pro-regular-svg-icons'

import { PromoCard } from '../components/PromoCard'
import Header from '../components/Header'
import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { WINDOW_WIDTH, WINDOW_HEIGHT } from '../config/dimensions'

import { TenpointCircle } from '../../assets'

const HomeScreen = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  const {
    state: { t }
  } = useContext(LocalizationContext)

  return (
    <View style={{ flex: 1 }}>
      <Header />
      <Card
        containerStyle={{
          width: '90%',
          borderRadius: 15,
          marginTop: '-15%',
          alignSelf: 'center',
          alignItems: 'center',
          padding: 4
        }}
        wrapperStyle={{ alignItems: 'center' }}>
        <View style={styles.topCardViewContainer}>
          <View style={styles.topCardCircleView}>
            <ImageBackground
              style={styles.topCardCircleImage}
              source={TenpointCircle}>
              <Text style={{ fontSize: 30, fontFamily: 'bookman' }}>1000</Text>
            </ImageBackground>
          </View>
          <View
            style={{
              width: '55%',
              justifyContent: 'center',
              alignItems: 'center',
              padding: 10
            }}>
            <Text
              style={{
                fontFamily: 'bookman',
                fontSize: 20,
                textAlign: 'center'
              }}>
              {t('YOU_CURRENTLY_HAVE')} 1000 tenpoint
            </Text>
          </View>
        </View>
      </Card>
      <ScrollView>
        <View style={styles.promoTitleContainer}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <FontAwesomeIcon icon={faFireAlt} color={'#FF4100'} size={18} />
            <Text style={styles.promoTitle}> {t('PROMOTION')}</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Button
              type='clear'
              title={t('VIEW_ALL')}
              containerStyle={{
                padding: 0,
                alignSelf: 'flex-end',
                marginRight: 5
              }}
              buttonStyle={{ padding: 0 }}
              titleStyle={{ padding: 0, textDecorationColor: '#0C7CC4' }}
            />
          </View>
        </View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={[
            {
              id: '1',
              name: 'Combo Covid-19: Nước ép + Bánh mỳ',
              price: '45.000',
              price_promo: '35.000',
              discount_percent: '11%',
              description: ''
            },
            { id: '2', name: 'Promotion #2' },
            { id: '3', name: 'Promotion #3' }
          ]}
          keyExtractor={item => item.id}
          renderItem={({ item }) => {
            return (
              <TouchableWithoutFeedback style={{ margin: 0 }}>
                <PromoCard item={item} />
              </TouchableWithoutFeedback>
            )
          }}
        />
      </ScrollView>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 100,
    backgroundColor: '#F7F6FB',
    width: '100%'
  },
  topCardViewContainer: {
    flexDirection: 'row',
    height: 0.18 * WINDOW_HEIGHT,
    width: 0.9 * WINDOW_WIDTH
  },
  topCardCircleView: {
    width: 0.9 * 0.45 * WINDOW_WIDTH,
    alignItems: 'center',
    justifyContent: 'center'
  },
  topCardCircleImage: {
    height: 0.15 * WINDOW_HEIGHT,
    width: 0.15 * WINDOW_HEIGHT,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  promoTitleContainer: {
    marginTop: 20,
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center'
  },
  promoTitle: {
    fontSize: 20,
    fontWeight: '900',
    color: '#FF4100'
  }
})

export default HomeScreen
