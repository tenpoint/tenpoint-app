import React, { useState, useContext, useLayoutEffect } from 'react'
import {
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  UIManager,
  View,
  Image,
  Text,
  TouchableWithoutFeedback
} from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { Card } from 'react-native-elements'
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell
} from 'react-native-confirmation-code-field'

import { Input, Button, Icon } from 'react-native-elements'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { Context as AuthContext } from '../context/AuthContext'
import { LanguageSelect } from '../components/LanguageSelect'
import PhoneVerification from '../components/PhoneVerification'
import { AppLogo } from '../../assets'

const PhoneVerificationScreen = ({ navigation }) => {
  const [code, setCode] = useState('')
  const {
    state: { phone },
    verifyCode,
    clearErrorMessage
  } = useContext(AuthContext)
  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  const CELL_COUNT = 6
  const [value, setValue] = useState('')
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT })
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue
  })

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  })

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        style={{ flex: 1, width: '100%' }}
        behavior='padding'>
        <View
          style={{
            flex: 2,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <Image style={{ width: 150, height: 150 }} source={AppLogo} />
          <Text
            style={{
              fontFamily: 'bookman',
              fontSize: 60,
              color: '#00CD6B',
              marginBottom: 15
            }}>
            tenpoint
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '400',
              marginTop: 10,
              color: 'rgba(0,0,0,0.6)'
            }}>
            {t('PHONE_VERIFICATION_TITLE')}
          </Text>
          <Card
            containerStyle={{
              width: '90%',
              borderRadius: 15
            }}
            wrapperStyle={{ alignItems: 'center' }}>
            <PhoneVerification code={code} setCode={setCode} />
          </Card>
          <Button
            title={t('CONTINUE')}
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            titleStyle={{ color: '#FFF' }}
            onPress={() => verifyCode({ phone, code })}
          />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
    backgroundColor: '#F7F6FB',
    width: '100%'
  },
  phoneContainer: {
    marginTop: 20,
    marginBottom: 20
  },
  phoneInputContainer: {
    //      borderBottomWidth: 0
    borderWidth: 1,
    borderRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.3)'
  },
  phoneInput: {
    textAlign: 'center'
  },
  phoneLabel: {
    fontWeight: '200',
    marginBottom: 5,
    marginLeft: 5
  },
  codeFiledRoot: { marginTop: 20 },
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 2,
    borderColor: '#00000030',
    textAlign: 'center'
  },
  focusCell: {
    borderColor: '#000'
  },
  buttonContainer: {
    marginTop: 15,
    width: '75%',
    borderRadius: 20
  },
  button: {
    height: 40,
    //borderRadius: 20,
    backgroundColor: '#00CD6B',
    borderRadius: 20
  }
})

export default PhoneVerificationScreen
