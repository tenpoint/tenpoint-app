import React, { useState, useContext } from 'react'
import {
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  UIManager,
  View
} from 'react-native'
import { SafeAreaView } from 'react-navigation'

import { Input, Button, Icon } from 'react-native-elements'

import { LanguageSelect } from '../components/LanguageSelect'
import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import { Context as AuthContext } from '../context/AuthContext'
import i18n from '../translations/i18n'

const SigninScreen = ({ navigation }) => {
  const [phone, setPhone] = useState('')
  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  const { state, auth } = useContext(AuthContext)

  return (
    <SafeAreaView style={styles.container}>
      <LanguageSelect
        languages={i18n.getAvailableLanguages()}
        onValueChange={changeAppLanguage}
      />

      <Input
        containerStyle={styles.phoneContainer}
        inputContainerStyle={styles.phoneInputContainer}
        inputStyle={styles.phoneInput}
        placeholder='09012345678'
        value={phone}
        onChangeText={setPhone}
        autoCorrect={false}
      />
      <Button
        title={t('CONTINUE')}
        type='outline'
        containerStyle={styles.buttonContainer}
        buttonStyle={styles.button}
        onPress={() => auth(phone)}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
    // justifyContent: 'center',
  },
  phoneContainer: {
    marginTop: 50
  },
  phoneInputContainer: {
    borderWidth: 1,
    borderRadius: 10
  },
  phoneInput: {
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop: 15,
    width: 200
  },
  button: {
    height: 40,
    borderRadius: 20
  }
})

//SignupScreen.navigationOptions = {
//  headerRight: () => (
//    <LanguageDropdown
//      languages={i18n.getAvailableLanguageDropdown()}
//      onValueChange={this.changeAppLanguage}
//    />
//  )
//}

SigninScreen.navigationOptions = () => {
  return {
    //headerTitle: i18n.t('SIGNUP_HEADER')
    //headerTransparent: true
  }
}

export default SigninScreen
