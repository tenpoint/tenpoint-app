import React from 'react'
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback
} from 'react-native'

import { Card } from 'react-native-elements'

import { SampleImages } from '../../assets'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height

// TODO: use image fetch from tenpoint API

export const PromoCard = ({ item }) => {
  return (
    <Card
      wrapperStyle={{ margin: 0, padding: 0 }}
      containerStyle={styles.promoCardContainer}>
      <Image
        source={SampleImages.promo}
        resizeMode='cover'
        style={styles.promoCardImage}
      />
      <Text style={styles.promoCardItemName}>{item.name}</Text>
      <View>
        <Text>{item.description}</Text>
      </View>
      <View style={styles.promoCardPriceView}>
        <Text
          style={{
            marginLeft: 5,
            color: '#FF4100',
            fontSize: 19,
            fontWeight: '600'
          }}>
          {item.price_promo}
        </Text>
        <Text
          style={{
            marginLeft: 5,
            textDecorationLine: 'line-through',
            textDecorationStyle: 'solid',
            fontSize: 16
          }}>
          {item.price}
        </Text>
      </View>
    </Card>
  )
}

const styles = StyleSheet.create({
  promoCardContainer: {
    width: 0.7 * WINDOW_WIDTH,
    height: 0.9 * WINDOW_WIDTH,
    borderRadius: 5,
    marginRight: 4,
    marginTop: 5,
    marginBottom: 5,
    padding: 0
  },
  promoCardImage: {
    width: 0.7 * WINDOW_WIDTH - 2,
    height: 0.6 * WINDOW_WIDTH,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  promoCardItemName: {
    fontSize: 15,
    fontWeight: '700',
    margin: 5
  },
  promoCardPriceView: {
    flexDirection: 'row',
    height: 0.1 * WINDOW_WIDTH,
    minHeight: 30,
    alignItems: 'flex-end'
  },
  popularCardContainer: {
    width: 0.5 * WINDOW_WIDTH,
    height: 0.5 * WINDOW_WIDTH,
    borderRadius: 5,
    marginRight: 4,
    marginTop: 5,
    marginBottom: 5,
    padding: 0
  },
  popularTitleContainer: {
    marginTop: 10,
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center'
  },
  popularTitle: {
    fontSize: 17,
    fontWeight: '900',
    color: '#00CD6B'
  }
})
