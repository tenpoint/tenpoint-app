import React from 'react'

import { getStatusBarHeight } from 'react-native-status-bar-height'
import { Header } from 'react-native-elements'

export default () => {
  return (
    <Header
      centerComponent={{
        text: 'tenpoint',
        style: {
          color: '#FFF',
          fontFamily: 'bookman',
          fontSize: 30,
          fontWeight: '500'
        }
      }}
      containerStyle={{
        height: '20%',
        alignItems: 'flex-start',
        marginTop: getStatusBarHeight(),
        backgroundColor: '#00CD6B'
      }}
    />
  )
}
