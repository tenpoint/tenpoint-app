import React, { useContext } from 'react'
import { Text, View, StyleSheet, AsyncStorage } from 'react-native'
import PickerSelect from 'react-native-picker-select'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'
import {
  getAvailableLanguageWithFlags,
  getAppLanguage
} from '../translations/i18n'

export const LanguageSelect = ({
  showLabel,
  backgroundColor,
  containerStyle
}) => {
  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  return (
    <View style={containerStyle}>
      {showLabel && (
        <Text style={styles.textInputLabel}>{t('LANGUAGE_SELECT_TITLE')}</Text>
      )}
      <PickerSelect
        useNativeAndroidPickerStyle={false}
        onValueChange={value => changeAppLanguage(value)}
        value={getAppLanguage()}
        items={getAvailableLanguageWithFlags()}
        placeholder={{ label: t('LANGUAGE_SELECT_PLACE_HOLDER'), value: null }}
        style={{
          paddingLeft: 0,
          marginTop: 5,
          fontSize: 20,
          inputAndroid: {
            fontSize: 18,
            color: 'rgba(0,0,0,0.7)',
            backgroundColor: backgroundColor ? backgroundColor : 'transparent'
          },
          width: 100,
          borderColor: '#000',
          backgroundColor: backgroundColor ? backgroundColor : 'transparent'
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  profileImageContainer: {
    padding: 3,
    marginTop: -44,
    width: 114,
    height: 114,
    borderRadius: 57,
    backgroundColor: '#FFF',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center'
  },
  profileDetailContainer: {
    width: '75%',
    alignSelf: 'center',
    marginTop: 20
  },
  textInputContainer: {
    marginTop: 5,
    marginBottom: 5
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.3)',
    color: 'rgba(0,0,0,0.7)',
    fontSize: 18,
    marginTop: 2
  },
  textInputLabel: {
    color: 'rgba(0,0,0,0.5)'
  }
})

//    <PickerSelect
//      onValueChange={value => onValueChange(value)}
//      items={languages}
//    />
