/*
Concept: https://dribbble.com/shots/5476562-Forgot-Password-Verification/attachments
*/
import React, { useState, useContext } from 'react'
import {
  Animated,
  Image,
  SafeAreaView,
  Text,
  View,
  StyleSheet
} from 'react-native'

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell
} from 'react-native-confirmation-code-field'

import { Context as LocalizationContext } from '../context/ExpoLocalizationContext'

const { Value, Text: AnimatedText } = Animated
const CELL_COUNT = 6
const imageSource = require('../../assets/images/verification_code_lock.png')

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0))
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1))
const animateCell = ({ hasValue, index, isFocused }) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      toValue: isFocused ? 1 : 0,
      duration: 250
    }),
    Animated.spring(animationsScale[index], {
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250
    })
  ]).start()
}

const PhoneVerification = ({ code, setCode }) => {
  const [value, setValue] = useState('')
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT })
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue
  })

  const {
    state: { t },
    changeAppLanguage
  } = useContext(LocalizationContext)

  const renderCell = ({ index, symbol, isFocused }) => {
    const hasValue = Boolean(symbol)
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [NOT_EMPTY_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR]
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR]
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [CELL_SIZE, CELL_BORDER_RADIUS]
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1]
          })
        }
      ]
    }

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    setTimeout(() => {
      animateCell({ hasValue, index, isFocused })
    }, 0)

    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}>
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    )
  }

  return (
    <SafeAreaView>
      <Image style={styles.icon} source={imageSource} />
      <Text style={styles.subTitle}>
        {t('PHONE_VERIFICATION_CODE_INPUT_LABEL')}
      </Text>

      <CodeField
        ref={ref}
        {...props}
        value={code}
        onChangeText={setCode}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFiledRoot}
        keyboardType='number-pad'
        renderCell={renderCell}
      />
    </SafeAreaView>
  )
}

const CELL_SIZE = 40
const CELL_BORDER_RADIUS = 8
const DEFAULT_CELL_BG_COLOR = '#fff'
const NOT_EMPTY_CELL_BG_COLOR = '#3557b7'
const ACTIVE_CELL_BG_COLOR = '#f7fafe'

const styles = StyleSheet.create({
  codeFiledRoot: {
    height: CELL_SIZE,
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'center'
  },
  cell: {
    margin: 5,
    height: CELL_SIZE,
    width: CELL_SIZE,
    lineHeight: CELL_SIZE - 5,
    ...Platform.select({ web: { lineHeight: 65 } }),
    fontSize: 30,
    textAlign: 'center',
    borderRadius: CELL_BORDER_RADIUS,
    color: '#3759b8',
    backgroundColor: '#fff',

    // IOS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    // Android
    elevation: 3
  },

  // =======================

  root: {
    minHeight: 800,
    padding: 20
  },
  title: {
    paddingTop: 10,
    color: '#000',
    fontSize: 25,
    fontWeight: '700',
    textAlign: 'center',
    paddingBottom: 40
  },
  icon: {
    width: 217 / 2.4,
    height: 158 / 2.4,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  subTitle: {
    paddingTop: 15,
    color: '#000',
    textAlign: 'center',
    fontSize: 16,
    color: 'rgba(0,0,0,0.6)'
  },
  nextButton: {
    marginTop: 40,
    borderRadius: 80,
    height: 80,
    backgroundColor: '#3557b7',
    justifyContent: 'center',
    minWidth: 360,
    marginBottom: 100
  },
  nextButtonText: {
    textAlign: 'center',
    fontSize: 20,
    color: '#fff',
    fontWeight: '700'
  }
})

export default PhoneVerification
