import axios from 'axios'
import { AsyncStorage } from 'react-native'

//let url = 'https://tenpoint.chococ.net/api/v1'
let url = 'http://04e67916.ngrok.io/api/v1'

const instance = axios.create({
  baseURL: url
})

instance.interceptors.request.use(
  async config => {
    const token = await AsyncStorage.getItem('token')
    if (token) {
      config.headers.Authorization = token
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

export default instance
