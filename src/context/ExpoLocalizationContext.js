import { AsyncStorage } from 'react-native'
import i18n, { DEFAULT_LANGUAGE } from '../translations/i18n'
import * as Localize from 'expo-localization'
import createDataContext from './createDataContext'

const APP_LANGUAGE = 'appLanguage'

const localizeReducer = (state = { t: scope => i18n.t(scope) }, action) => {
  switch (action.type) {
    case 'change_language':
      return { t: action.payload }
    default:
      return state
  }
}

const initializeAppLanguage = dispatch => async () => {
  const currentAppLanguage = await AsyncStorage.getItem(APP_LANGUAGE)

  if (!currentAppLanguage) {
    let localeCode = DEFAULT_LANGUAGE
    const supportedLocaleCodes = i18n.getAvailableLanguageCodes()
    const phoneLocaleCodes = Localize.locales
    phoneLocaleCodes.some(code => {
      if (supportedLocaleCodes.includes(code)) {
        localeCode = code
        return true
      }
    })
    i18n.locale = localCode
    dispatch({ type: 'change_language', payload: scope => i18n.t(scope) })
  } else {
    i18n.locale = currentAppLanguage
    dispatch({ type: 'change_language', payload: scope => i18n.t(scope) })
  }
}

const changeAppLanguage = dispatch => async language => {
  i18n.locale = language
  dispatch({ type: 'change_language', payload: scope => i18n.t(scope) })
  AsyncStorage.setItem(APP_LANGUAGE, language)
}

export const { Provider, Context } = createDataContext(
  localizeReducer,
  { initializeAppLanguage, changeAppLanguage },
  { t: scope => i18n.t(scope) }
)
