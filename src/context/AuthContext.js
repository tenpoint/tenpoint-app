import { AsyncStorage } from 'react-native'
import createDataContext from './createDataContext'
import { navigate } from '../navigations/navigationRef'
import tenpointApi from '../api/tenpoint'

const authReducer = (state, action) => {
  switch (action.type) {
    case 'auth':
      return { ...state, ...action.payload, errorMessage: '' }
    case 'signin':
      return { ...state, ...action.payload, errorMessage: '' }
    case 'signout':
      return { token: null, errorMessage: '' }
    case 'add_error':
      return { ...state, errorMessage: action.payload }
    case 'clear_error_message':
      return { token: null, errorMessage: '' }
    default:
      return state
  }
}

const tryLocalSignin = dispatch => async () => {
  const token = await AsyncStorage.getItem('token')
  if (token) {
    dispatch({ type: 'signin', payload: token })
    navigate('Main')
  } else {
    navigate('Signup')
  }
}

const isSignedIn = async () => {
  const token = await AsyncStorage.getItem('token')
  if (token) {
    return true
  } else {
    return false
  }
}

const clearErrorMessage = dispatch => () => {
  dispatch({ type: 'clear_error_message' })
}

const auth = dispatch => async ({ phone }) => {
  try {
    const { status, data } = await tenpointApi.post('/auth', { phone })
    if (status !== 200) throw 'status !== 200'
    else {
      dispatch({ type: 'auth', payload: { phone } })
      navigate('PhoneVerification')
    }
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Có lỗi xảy ra. Vui lòng thử lại.'
    })
  }
}

const verifyCode = dispatch => async ({ phone, code }) => {
  try {
    const { status, data } = await tenpointApi.post('/verify_auth_code', {
      phone,
      code
    })

    if (status !== 200) throw 'status !== 200'
    else {
      await AsyncStorage.setItem('token', data.token)
      navigate('Main')
    }
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Có lỗi xảy ra. Vui lòng thử lại.'
    })
  }
}

const signup = dispatch => async ({ phone, password }) => {
  try {
    //const response = await tenpointApi.post('/signup', { phone, password })
    //await AsyncStorage.setItem('token', response.data.qrcode)

    navigate('PhoneVerification')
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Có lỗi xảy ra. Vui lòng thử lại.'
    })
  }
}

const signin = dispatch => async ({ phone, password }) => {
  try {
    const response = await tenpointApi.post('/signin', { phone, password })
    await AsyncStorage.setItem('token', response.token)
    dispatch({ type: 'signin', payload: response.token })

    navigate('Main')
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Số điện thoại hoặc mật khẩu không đúng.'
    })
  }
}

const signout = dispatch => async () => {
  await AsyncStorage.removeItem('token')
  dispatch({ type: 'signout' })

  navigate('Auth')
}

export const { Provider, Context } = createDataContext(
  authReducer,
  {
    isSignedIn,
    auth,
    verifyCode,
    signup,
    signin,
    signout,
    clearErrorMessage,
    tryLocalSignin
  },
  { token: null, errorMessage: '' }
)
