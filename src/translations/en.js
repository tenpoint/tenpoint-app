export default {
  I_ACCEPT_THE: 'I accept the',
  CONTINUE: 'CONTINUE',
  LANGUAGE_SELECT_TITLE: 'Language',
  LANGUAGE_SELECT_PLACE_HOLDER: 'Select language',
  NAME: 'Name',
  MOBILE_NUMBER: 'Mobile number',
  PHONE_VERIFICATION_TITLE: 'Verify mobile number',
  PHONE_VERIFICATION_CODE_INPUT_LABEL: 'Enter SMS verification code',
  PROMOTION: 'Promotion',
  SCANNER: 'Scanner',
  SIGN_OUT_BTN_TEXT: 'SIGN OUT',
  SIGN_IN_REGISTER_SCREEN_TITLE: 'Sign in | Register',
  SIGN_UP_HEADER: 'Enter phone number',
  TERMS_AND_CONDITIONS_TEXT: 'Terms and Conditions',
  THE: 'the',
  VIEW_ALL: 'View all',
  YOU_CURRENTLY_HAVE: 'You currently have'
}
