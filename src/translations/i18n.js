import i18n from 'i18n-js'

import en from './en'
import vn from './vn'

export const DEFAULT_LANGUAGE = 'vn'

i18n.locale = 'vn'

export const getAvailableLanguageCodes = () => {
  return Object.keys(i18n.translations)
}

export const getAvailableLanguages = () => {
  return [
    { value: 'vn', label: 'Tiếng Việt' },
    { value: 'en', label: 'English' }
  ]
}

export const getAppLanguage = () => {
  return i18n.locale
}

export const getAvailableLanguageWithFlags = () => {
  return [
    { value: 'vn', label: '🇻🇳 Tiếng Việt' },
    { value: 'en', label: '🇺🇸 English' }
  ]
}

i18n.translations = {
  en: en,
  vn: vn
}

export default i18n
