export default {
  I_ACCEPT_THE: 'Tôi đồng ý',
  CONTINUE: 'TIẾP TỤC',
  LANGUAGE_SELECT_TITLE: 'Ngôn ngữ',
  LANGUAGE_SELECT_PLACE_HOLDER: 'Chọn ngôn ngữ',
  MOBILE_NUMBER: 'Số điện thoại',
  NAME: 'Tên',
  PHONE_VERIFICATION_TITLE: 'Xác nhận số điện thoại',
  PHONE_VERIFICATION_CODE_INPUT_LABEL: 'Nhập mã xác thực từ SMS',
  PROMOTION: 'Khuyến mãi',
  SCANNER: 'Quét mã',
  SIGN_OUT_BTN_TEXT: 'Đăng xuất',
  SIGN_IN_REGISTER_SCREEN_TITLE: 'Đăng nhập | Đăng ký',
  SIGN_UP_HEADER: 'Nhập số điện thoại',
  TERMS_AND_CONDITIONS_TEXT: 'Điều khoản sử dụng',
  THE: '',
  VIEW_ALL: 'Xem thêm',
  YOU_CURRENTLY_HAVE: 'Bạn đang có'
}
