export const MainColors = {
  red: '#FF4100',
  green: '#00CD6B',
  blue: '#0C7CC4',
  orange: '#FF8E00',
  text: 'rgba(0,0,0,0.7)'
}
