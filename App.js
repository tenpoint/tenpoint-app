import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { AppLoading } from 'expo'
import * as Font from 'expo-font'

import { Provider as AuthProvider } from './src/context/AuthContext'
import { Provider as LocalizeProvider } from './src/context/ExpoLocalizationContext'
import App from './src/navigations/AppNavigator'

export default props => {
  const [isLoadingComplete, setLoadingComplete] = useState(false)
  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    )
  } else {
    return (
      <LocalizeProvider>
        <AuthProvider>
          <App />
        </AuthProvider>
      </LocalizeProvider>
    )
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      bookman: require('./assets/fonts/BOOKOS.ttf')
    })
  ])
}

function handleLoadingError(error) {
  console.warn(error)
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true)
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
